﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enumerable
{
    class ListNode : IEnumerable
    {
        public ListNode(int item)
        {
            value = item;
        }

        public int value;
        public ListNode next;
        private ListNode _lastNode;

        public void Add(int item)
        {
            if (_lastNode == null)
            {
                next = new ListNode(item);
                _lastNode = next;
            }
            else
            {
                _lastNode.next = new ListNode(item);
                _lastNode = _lastNode.next;
            }
        }
        public override string ToString() => value.ToString();

        public IEnumerator GetEnumerator() => new Enumerator(this);
        private class Enumerator : IEnumerator
        {
            public Enumerator(ListNode root)
            {
                _node = root;
            }

            public object Current { get; private set; }
            private ListNode _node;

            public bool MoveNext()
            {
                if (_node == null)
                {
                    return false;
                }
                Current = _node.value;
                _node = _node.next;
                return true;
            }

            public void Reset()
            {
                _node = null;
            }
        }
    }
}
