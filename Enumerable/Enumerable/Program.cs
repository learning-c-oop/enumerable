﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            var listNode = new ListNode(10);
            listNode.Add(20);
            listNode.Add(30);
            listNode.Add(40);
            listNode.Add(50);

            //IEnumerator enumerator = listNode.GetEnumerator();
            //while (enumerator.MoveNext())
            //{
            //    int item = (int)enumerator.Current;
            //}
            //enumerator.Reset();
            foreach (int item in listNode)
            {

            }

            Console.ReadKey();
        }
    }
}
